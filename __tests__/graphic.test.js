import React from "react";
import ReactDOM from "react-dom";
import Graphic from "../src/components/graphic";

import { render } from "@testing-library/react";

// automatically unmount and cleanup DOM after the test is finished.
afterEach(cleanup);

it("render Ghraphic without crashing", () => {
  const div = document.createElement("div");
  ReactDOM.render(<Graphic></Graphic>, div);
});

it("renders button correctly", () => {
  render(<Graphic></Graphic>);
});

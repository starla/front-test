# Proyecto WEB (test) para la integracion de oAuth y construcción de grafica a partir de datos consultados a traves de GraphQL a un endpoint dado.

## Instalación

Para la correcta ejecucion del proyecto se instalan las dependencias con el siguiente comando en la carpeta raiz del mismo

```
$npm install
```

una vez instaladas las dependencias se ejecuta el proyecto con el comando

```
$npm run start
```

o

```
$yarn start
```

Esto lanzara al navegador el contenido inicial de nuestro proyecto a traves de la url `http://localhost:3000`

una vez en ejecucion en el proyecto tenemos solo 2 ruta

## Login

Esta ruta '/login'

Esta contiene un boton para acceder a nuestro contenido privado solo y solo a traves de la sesion de facebook

comparto tabla de usuario de prueba para este proyecto

| Nombre                | Identificador de usuario | Correo electrónico             |
| --------------------- | ------------------------ | ------------------------------ |
| Lisa test gbm uno     | 100250978152358          | lisa_iscjarw_uno@tfbnw.net     |
| Margaret test gbm dos | 100549878121368          | margaret_qvdlaqs_dos@tfbnw.net |
| Tyler test gbm tres   | 106194220881906          | tyler_lqvcqhy_tres@tfbnw.net   |
| Will test gbm cuatro  | 108773187287671          | will_hniqxau_cuatro@tfbnw.net  |

El password para los 4 usuarios es el mismo
pass: `Apples01`

## Dashoard

Esta ruta '/dashboard' se lanza despues de levantar la sesion con facebook
a travez de nuestra aplicación.

El conteniod general de dashboar es mostrar una grafica de barras con loS
dstos obtenidos a traves de nuestro middleware previamente levantado y consultado al API
que nos compartieron.

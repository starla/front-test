import fetchGraph from "../misc/fetcher";

const triggerFetchDashboard = (dispatch, state, url, header) => {
  fetchGraph(url, header).then(payload =>
    dispatch({ type: "GET_DASHBOARD", payload })
  );
};

export default (dispatch, state) => ({
  getDataDashboard: (url, header) => {
    return triggerFetchDashboard(dispatch, state, url, header);
  },
  getFacebookSession: profile => {
    dispatch({ type: "GET_FACEBOOK_LOGIN", profile });
  }
});

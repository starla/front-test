export default (state, action) => {
  if (action.type === "GET_DASHBOARD") {
    return {
      ...state,
      dashboard: {
        ...action.payload.dataGraph
      }
    };
  }
  if (action.type === "GET_FACEBOOK_LOGIN") {
    return {
      ...state,
      login: { ...action.profile }
    };
  }
  console.log(state);
  return state;
};

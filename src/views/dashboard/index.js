import React, { useEffect, useReducer, useState } from "react";
import { stringify } from "query-string";
import reducer from "../../providers/reducer";
import actionsCreator from "../../providers/actions";
import Header from "../../components/header";
import Graphic from "../../components/graphic";

/**
 * Dashboard Component
 *
 * @name Dashboard
 * @description Vista que se usa para renderizar el contenifo privade
 * que se construye a travez de una ruta privada
 *
 * @param { Object } props.location recibe state a travez del location.state
 *
 * @example
 *  <PrivateRoute path="/dashboard" component={Dashboard} />
 *
 * @returns {React.Component}
 */

const queryAll = `query get_query{
  viewer{
    dataIPC {
      Fecha
      Precio
      Porcentaje
      Volumen
    }
  }
}`;

const vars = `{
  "Fecha": "",
  "Precio": "",
  "Porcentaje": "",
  "Volumen": ""
}`;

const Dashboard = props => {
  const login = (props.location && props.location.state) || null;

  const [state, dispatch] = useReducer(reducer, { dashboard: {} });
  const actions = actionsCreator(dispatch, state);
  const { dashboard = {} } = state;
  const { dataIPC = null } = dashboard;
  const [data, setData] = useState(dataIPC);

  useEffect(() => {
    const url =
      (process.env.NODE_ENV !== "production" &&
        "http://localhost:2002/_graphql") ||
      "http://localhost:2002/_graphql";
    const header = {
      mode: "cors",
      credentials: "omit",
      method: "POST",
      headers: {
        "Content-type": "application/x-www-form-urlencoded"
      },
      body: stringify({
        query: queryAll,
        variables: vars
      })
    };
    actions.getDataDashboard(url, header);
  }, [data]);

  return (
    <div>
      <Header login={login} />
      <Graphic graph={dashboard.dataIPC} setData={setData} />
    </div>
  );
};

export default Dashboard;

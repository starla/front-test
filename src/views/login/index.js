import React, { useReducer, useEffect } from "react";
import { Redirect } from "react-router-dom";
import styles from "./index.module.sass";
import reducer from "../../providers/reducer";
import actionsCreator from "../../providers/actions";
import FacebookLogin from "react-facebook-login";

/**
 * Login Component
 *
 * @name Login
 * @description Vista donde se construye el Login para el acceso por Facebook oAuth
 *
 * @param { } Se contruye a traves de una ruta
 *
 * @example
 *  <Route path="/login" component={Login} />
 *
 * @returns {React.Component}
 */

const Login = () => {
  const [state, dispatch] = useReducer(reducer, { login: null });
  const actions = actionsCreator(dispatch, state);

  const responseFacebook = response => {
    actions.getFacebookSession(response);
  };

  const veriedSession = () => {};
  useEffect(() => {}, [state]);

  return (
    <div id={styles.login}>
      {state.login && (
        <Redirect
          to={{
            pathname: "/dashboard",
            state: { login: state.login }
          }}
        />
      )}
      <FacebookLogin
        appId="589351021829910"
        autoLoad={false}
        fields="name,email,picture"
        onClick={veriedSession}
        callback={responseFacebook}
      />
    </div>
  );
};

export default Login;

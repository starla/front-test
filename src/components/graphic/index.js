import styles from "./index.module.sass";
import React from "react";

/**
 * Graphic Component
 *
 * @name Graphic
 * @description Componente para renderizar la grafica de IPC
 * parametros para el componente de Graphic
 *
 * @param { Array } props.graph lista de objetos que utiliza el componente para construir la grafica
 *
 * @example
 *  <Graphic  graph={data} >
 *  </BoxBanner>
 *
 * @returns {React.Component}
 */

const Graphic = ({ graph }) => {
  return (
    <div id={styles.graphic}>
      <div id={styles.containerGraphic}>
        <div id={styles.scrollLines}>
          {graph &&
            graph.map((bar, index) => {
              const { Fecha, Precio, Porcentaje, Volumen } = bar;
              const time = new Date(Fecha);
              const date = time.getDate();
              const month = time.getMonth();
              const year = time.getFullYear();
              const hours = time.getHours();
              const minutes = time.getMinutes();
              const price = bar.Precio / 100;
              return (
                <div
                  key={index}
                  className={styles.lineBar}
                  style={{
                    left: index * 5,
                    height: `${price}px`
                  }}
                >
                  <div className={styles.hour}>{`${hours}:${minutes}Hrs.`}</div>
                  <div className={styles.blob}>
                    <div className={styles.indicatorArrowa}></div>
                    <div className={styles.indicatorArrowb}></div>
                    <p className={styles.date}>
                      <b>Fecha</b>: {`${date}-${month + 1}-${year}`}
                    </p>
                    <p className={styles.price}>
                      <b>Precio:</b> ${Precio}
                    </p>
                    <p className={styles.percent}>
                      <b>porcentaje:</b> {Porcentaje}%
                    </p>
                    <p className={styles.size}>
                      <b>Volumen:</b> {Volumen}
                    </p>
                  </div>
                </div>
              );
            })}
        </div>
      </div>
    </div>
  );
};

export default Graphic;

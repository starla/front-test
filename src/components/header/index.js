import React from "react";
import styles from "./index.module.sass";

/**
 * Header Component
 *
 * @name Header
 * @description Componente Header que renderiza información general del usuario
 *
 * @param { Object } props.login resibe login (objeto de la sesión de facebook)
 * para renderizar el nombre y la imagen del usuario que inisio sesión
 *
 * @example
 *  <Header login={login} />
 *
 * @returns {React.Component}
 */

const Header = ({ login }) => {
  const picture =
    (login &&
      login.login &&
      login.login.picture &&
      login.login.picture.data &&
      login.login.picture.data.url) ||
    null;

  return (
    <div id={styles.header} className={"b-header"}>
      <div id={styles.logo}>
        <i className="material-icons" id={styles.logoIcon}>
          control_camera
        </i>
        <span>{login && login.login && login.login.name}</span>
      </div>
      <div id={styles.poster}>
        <div id={styles.profile}>
          {(picture && <img alt="" src={picture} />) || (
            <i className="material-icons" id={styles.logoAcount}>
              account_circle
            </i>
          )}
        </div>
      </div>
    </div>
  );
};

export default Header;

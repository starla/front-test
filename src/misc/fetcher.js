const mapResponseFn = res => {
  let dataGraph = null;
  try {
    dataGraph = res && res.data && res.data.viewer;
  } catch (e) {
    console.log(e);
  }
  return {
    dataGraph
  };
};

const fetchGraph = (url = "", header = {}) => {
  return fetch(url, header)
    .then(res => res.json())
    .then(mapResponseFn)
    .catch(error => {
      console.log(error);
    });
};

export default fetchGraph;
